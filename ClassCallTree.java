import java.util.*;
import java.io.*;




/**
*   Determines the call tree of a given class
*   @author Julian Carrivick
*   @studentid 16164442
*   @subject Software Metrics 400
*   @date May 2014
*/
public class ClassCallTree
{
    ClassFile classFile;
    Stack<ClassCallTreeNode> callStack;
    ArrayList<ClassCallTreeNode> thisClassMethods, calledMethods;
    ArrayList<String> usedClasses;




    /**
    *   Returns a new instance of ClassCallTree
    */
    public ClassCallTree()
    {
        /* Initialise all class fields */
        usedClasses = new ArrayList<String>();
        
        callStack = new Stack<ClassCallTreeNode>();
        thisClassMethods = new ArrayList<ClassCallTreeNode>();
        calledMethods = new ArrayList<ClassCallTreeNode>();
        
        classFile = null;
    }


    /**
    *   Returns a new instance of ClassCallTree given a ClassFile
    */
    public ClassCallTree(ClassFile inClassFile)
    {
        /* Initialise all class fields */
        usedClasses = new ArrayList<String>();
        
        callStack = new Stack<ClassCallTreeNode>();
        thisClassMethods = new ArrayList<ClassCallTreeNode>();
        calledMethods = new ArrayList<ClassCallTreeNode>();
        
        classFile = inClassFile;
    }


    /**
    *   setClassFile
    *   Changes the classfile.
    */
    public void setClassFile(ClassFile inClassFile)
    {
        classFile = inClassFile;
    }


    /**
    *   toString
    *   Outputs the call tree as a string.
    */
    public String toString()
    {
        StringBuilder s = new StringBuilder();

        for (ClassCallTreeNode node : thisClassMethods) 
        {
            s.append("Call Tree:\n");
            s.append(recurseToString(node, 1));
        }

        s.append("\n" + usedClasses.size() + " classes used.\n");
        s.append(calledMethods.size() + " unique methods called.");
        s.append("\n------------------------------------------\n");

        return s.toString();
    }


    /**
    *   recurseToString
    *   Recursively returns the call tree from a particular node and depth.
    */
    private String recurseToString(ClassCallTreeNode inNode, int depth)
    {
        StringBuilder s = new StringBuilder();

        s.append(inNode.toString());

        for (ClassCallTreeNode node :  inNode.children)
        {
            for (int i = 0; i < depth; i++) 
            {
                s.append("|  "); 
            }

            s.append(recurseToString(node, depth + 1));
        }

        return s.toString();
    }


    /**
    *   buildCallTree
    *   Builds the call tree for every method in the class.
    */
    public void buildCallTree() throws ClassFileParserException
    {
        ClassCallTreeNode node;
        ClassMethods methods;

        methods = classFile.getMethods();

        /* For each method in the class, recursively build the call tree
        *   for that method */
        for (ClassMethod method : methods) 
        {
            node = new ClassCallTreeNode(method);

            node.className = classFile.getClassName();
            thisClassMethods.add(node);

            recurseCallTree(node);
        }
    }


    /**
    *   buildCallTree
    *   Builds a call tree given a specific method.
    */
    public void buildCallTree(ClassMethod inMethod) 
        throws ClassFileParserException
    {
        ClassCallTreeNode node = new ClassCallTreeNode(inMethod);

        node.className = classFile.getClassName();
        thisClassMethods.add(node);

        recurseCallTree(node);
    }


    /**
    *   recurseCallTree
    *   Recursively builds a call tree for the given node.
    */
    private void recurseCallTree(ClassCallTreeNode inNode) 
        throws InvalidConstantPoolIndex, InvalidAttributeException
    {
        CodeAttribute code;
        ClassCallTreeNode tempNode;
        ArrayList<ConstantRef> methodRefsCalled;

        if (!usedClasses.contains(inNode.className))
        {
            usedClasses.add(inNode.className);
        }

        if (!calledMethods.contains(inNode))
        {
            calledMethods.add(inNode);
        }

        callStack.push(inNode);

        if (inNode.method != null)
        {
        /* In the code attribute, get the methods that are called from this 
        *   method and recurse through those methods.*/
            methodRefsCalled = inNode.getMethod().getMethodRefsCalled();

            for (ConstantRef methodRef : methodRefsCalled) 
            {
                tempNode = getNodeFromMethodRef(methodRef);

                /* If this method is already on the call stack, it is
                    recursive 
                    I would prefer to use callStack.contains(tempNode) but
                    for some reason it isn't finding the node 
                */
                for (ClassCallTreeNode callM : callStack) 
                {
                    if (callM.equals(tempNode))
                    {
                        tempNode.recursive = true;
                    }   
                }

                if (!inNode.hasChild(tempNode))
                {
                    inNode.children.add(tempNode);

                    if ( !tempNode.recursive && !tempNode.abstr )
                    {
                        /* Only recurse if the method doesn't already 
                            exist in this node's children and isn't
                            recursive or abstract 
                        */
                            recurseCallTree(tempNode);
                    }
                }
            }
        }

        callStack.pop();
    }


    /**
    *   getNodeFromMethodRef
    *   Returns a new ClassCallTreeNode given a methodReference (from the 
    *   constant pool).
    *   The ClassCallTreeNode will have a null method if the imported 
    *   methodRef cannot be found in the ClassFile or in any ClassFile in this
    *   file.
    */
    private ClassCallTreeNode getNodeFromMethodRef(ConstantRef methodRef)
        throws InvalidConstantPoolIndex
    {
        ClassMethod method = null;
        ClassCallTreeNode newNode = null;

        newNode = new ClassCallTreeNode();

        newNode.methodRef = methodRef;
        newNode.className = methodRef.getClassName();

        /* If a class is a enum, then the classname is represented as in a 
            data type instead. when Opcode.values() is called it calls a 
            Opcode[].clone() method. */
        if (methodRef.getClassName().charAt(0) == '[')
        {
            method = new ClassMethod();

            /* For the special case when the 'class' is a array */
            try
            {
                newNode.className = 
                    method.getDataType(methodRef.getClassName());
            }
            catch (InvalidClassMethodException e)
            {
                newNode.className = "<unknown>";
            }
        }
        
        /* If the methodRef is in this class, find the ClassMethod and add it 
        *   to the node.
        *   Otherwise, try and parse the relevent class file and find it there
        */
        if (methodRef.getClassName().equals(classFile.getClassName()))
        {
            method = getMethodFromThisClass(methodRef);
            newNode.setMethod(method);
        }
        else
        {
            try
            {
                method = getMethodFromOtherClass(methodRef);
                newNode.setMethod(method);
            }
            catch (IOException e)
            {
                newNode.notFound = true;
            }
            catch (ClassFileParserException e)
            {
                newNode.invalid = true;
            }
        }

        return newNode;
    }


    /**
    *   getMethodFromOtherClass
    *   Given a ConstantRef, attempt to generate a method object for it
    *   by parsing the relevent class file.
    */
    private ClassMethod getMethodFromOtherClass(ConstantRef methodRef)
        throws IOException, ClassFileParserException
    {
        ClassFile tempClassFile = null;
        ClassMethod tempMethod = null;

        tempClassFile = new ClassFile(methodRef.getClassName() + ".class");

        for (ClassMethod method : tempClassFile.getMethods()) 
        {
            if (methodRef.equals(method))
            {
                tempMethod = method;
            }
        }

        return tempMethod;
    }


    /**
    *   getMethodFromThisClass
    *   Given a ConstantRef, find the method from the class file already in
    *   memory.
    */
    private ClassMethod getMethodFromThisClass(ConstantRef methodRef)
    {
        ClassMethod tempMethod = null;

        for (ClassMethod method : classFile.getMethods())
        {
            if (method.equals(methodRef.getName(), methodRef.getType()))
            {
                tempMethod = method;
            }
        }

        return tempMethod;
    }


    /**
    *   A simple class to represent a node in the call tree
    */
    private class ClassCallTreeNode
    {
        /* Need a reference to the method as well as the method reference in
            case the class file cannot be found in order to determine the
            parameters for the method */
        private ClassMethod method;
        private ConstantRef methodRef;

        private boolean abstr, recursive, notFound, invalid;
        private ArrayList<ClassCallTreeNode> children;
        private String className;


        /**
        *   Returns a new instance of the class
        */
        public ClassCallTreeNode()
        {
            method = null;
            abstr = recursive = notFound = false;
            className = null;
            
            children = new ArrayList<ClassCallTreeNode>();
        }


        /**
        *   Returns a new instance of the class given a method
        */
        public ClassCallTreeNode(ClassMethod inMethod)
        {
            recursive = notFound = false;   
            className = null;
            
            children = new ArrayList<ClassCallTreeNode>();
            
            method = inMethod;

            if (inMethod != null)
            {
                abstr = inMethod.isAbstract();     
            }
        }
        

        /**
        *   Returns a new instance of the class given a ConstantRef
        */
        public ClassCallTreeNode(ConstantRef inMethodRef)
        {
            recursive = notFound = false;   
            className = null;
            
            children = new ArrayList<ClassCallTreeNode>();

            methodRef = inMethodRef;
        }


        /**
        *   setMethod
        *   Changes the method that this node refers to.
        */
        public void setMethod(ClassMethod inMethod)
        {
            method = inMethod;

            if (inMethod != null)
            {
                abstr = inMethod.isAbstract();     
            }
        }


        /**
        *   getMethod
        *   Returns the method at this node
        */
        public ClassMethod getMethod()
        {
            return this.method;
        }


        /**
        *   getMethodRef
        *   Returns the ConstantRef for this method
        */
        public ConstantRef getMethodRef()
        {
            return this.methodRef;
        }


        /**
        *   hasChild
        *   Returns whether this node has the specified input node as a child
        */
        public boolean hasChild(ClassCallTreeNode inNode)
        {
            boolean hasTheChild = false;

            for (ClassCallTreeNode child : children) 
            {   
                if (child.equals(inNode))
                {
                    hasTheChild = true;
                }   
            }

            return hasTheChild;
        }


        /**
        *   equals
        *   If this node has the same className and same method then it is 
        *   equal.
        */
        public boolean equals(ClassCallTreeNode inNode)
        {
            boolean equal;

            equal = className.equals(inNode.className);

            if (this.method != null && inNode.getMethod() != null)
            {
                equal = equal && this.method.equals(inNode.getMethod());  
            }
            else if (this.method != null && inNode.getMethod() == null)
            {
                equal = equal && this.method.equals(inNode.getMethodRef());
            }
            else if (this.method == null && inNode.getMethod() != null)
            {
                equal = equal && inNode.getMethod().equals(this.methodRef);
            }
            else
            {
                equal = this.methodRef.equals(inNode.getMethodRef());
            }

            return equal;
        }


        /**
        *   toString
        *   Returns the node as a formatted string
        */
        public String toString()
        {
            StringBuilder s = new StringBuilder();
            String[] tempStrArr;

            tempStrArr = className.split("/");

            s.append(tempStrArr[tempStrArr.length - 1] + ".");

            if (method == null)
            {
                s.append(methodRef.toTypeString());
            }
            else
            {
                s.append(method.toTypeString());
            }

            if (abstr)
            {
                s.append(" [abstract]");
            }
            
            if (recursive)
            {
                s.append(" [recursive]");
            }
            
            if (notFound)
            {
                s.append(" [missing]");
            }

            if (invalid)
            {
                s.append(" [invalid class file]");
            }

            s.append("\n");

            return s.toString();
        }
    }
}