Call Tree:
TestClass.methodA()
|  TestClass.methodB()

1 classes used.
2 unique methods called.
------------------------------------------

Call Tree:
TestClass.methodF()
|  TestClass.methodE()
|  TestClass.methodE(int)
|  TestClass.methodE(char)

1 classes used.
4 unique methods called.
------------------------------------------

Call Tree:
TestClass.methodG()
|  Integer.Constructor(int) [missing]

2 classes used.
2 unique methods called.
------------------------------------------

Call Tree:
TestClass.methodH()
|  TestClass.methodH() [recursive]

1 classes used.
1 unique methods called.
------------------------------------------

Call Tree:
TestClass.methodI()
|  TestClass.methodJ()
|  |  TestClass.methodI() [recursive]

1 classes used.
2 unique methods called.
------------------------------------------

Call Tree:
TestClass.methodK()
|  Integer.getInteger(String) [missing]

2 classes used.
2 unique methods called.
------------------------------------------

Call Tree:
TestClass.methodL()
|  TestClass.methodA()
|  |  TestClass.methodB()
|  TestClass.methodF()
|  |  TestClass.methodE()
|  |  TestClass.methodE(int)
|  |  TestClass.methodE(char)
|  TestClass.methodG()
|  |  Integer.Constructor(int) [missing]
|  TestClass.methodH()
|  |  TestClass.methodH() [recursive]
|  TestClass.methodI()
|  |  TestClass.methodJ()
|  |  |  TestClass.methodI() [recursive]
|  TestClass.methodK()
|  |  Integer.getInteger(String) [missing]
|  String.Constructor(String) [missing]
|  Integer.getInteger(String) [missing]
|  Double.parseDouble(String) [missing]
|  String.substring(int, int) [missing]

4 classes used.
18 unique methods called.
------------------------------------------

Call Tree:
TestClass2.methodA() [abstract]

1 classes used.
1 unique methods called.
------------------------------------------

