import java.io.*;




/**
 * Parses and stores a Java .class file. Parsing is currently incomplete.
 *
 * @author David Cooper
 * @edited Julian Carrivick
 * @studentid 16164442
 * @subject Software Metrics 400
 * @date May 2014
 */
public class ClassFile
{
    private String filename;
    private long magic;
    private int minorVersion;
    private int majorVersion;
    private ConstantPool constantPool;
    private int accessFlags;
    private ConstantClass thisClass, superClass;
    private ClassInterfaces interfaces;
    private ClassFields fields;
    private ClassMethods methods;
    private Attributes attributes;


    /**
     * Parses a class file an constructs a ClassFile object. At present, this
     * only parses the header and constant pool.
     */
    public ClassFile(String filename) throws ClassFileParserException,
                                             IOException
    {
        DataInputStream dis =
            new DataInputStream(new FileInputStream(filename));

        this.filename = filename;
        magic = (long)dis.readUnsignedShort() << 16 | dis.readUnsignedShort();
        minorVersion = dis.readUnsignedShort();
        majorVersion = dis.readUnsignedShort();
        constantPool = new ConstantPool(dis);
        accessFlags = dis.readUnsignedShort();

        thisClass = 
            (ConstantClass)constantPool.getEntry(dis.readUnsignedShort());
            
        superClass = 
            (ConstantClass)constantPool.getEntry(dis.readUnsignedShort());

        interfaces = new ClassInterfaces(dis, constantPool);
        fields = new ClassFields(dis, constantPool);
        methods = new ClassMethods(dis, constantPool);
        attributes = new Attributes(dis, constantPool);
    }


    /** 
    *   getMethods
    *   Returns the methods associated with this class
    */
    public ClassMethods getMethods()
    {
        return methods;
    }


    /**
    *   getClassName
    *   Returns the name of this class 
    */
    public String getClassName()
    {
        return thisClass.getName();
    }


    /**
    *   getCPEntry
    *   Returns the CPEntry associated with a given index. 
    */
    public CPEntry getCPEntry(int index) throws InvalidConstantPoolIndex
    {
        return constantPool.getEntry(index);
    }


    /** 
    *   toString
    *   Returns the contents of the class file as a formatted String.
    */
    public String toString()
    {
        return String.format(
            "Filename: %s\n" +
            "Magic: 0x%08x\n" +
            "Class file format version: %d.%d\n\n" +
            "Constant pool:\n\n%s\n" +
            "Access Flags: 0x%04x\n" +
            "Class Name: %s\n" +
            "Super Class Name: %s\n" +
            "Interfaces:\n%s\n" + 
            "Fields:%s\n" +
            "Methods:%s\n" +
            "Attributes:%s\n",
            filename, magic, majorVersion, minorVersion, constantPool, 
            accessFlags, thisClass.getName(), superClass.getName(), 
            interfaces, fields, methods, attributes );
    }    
}