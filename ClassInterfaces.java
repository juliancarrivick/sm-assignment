import java.io.*;

/**
*   Parses and stores the interfaces in a class file.
*   @author Julian Carrivick
*   @studentid 16164442
*   @subject Software Metrics 400
*   @date May 2014
*/
public class ClassInterfaces
{
    private int[] interfaces;
    private String[] interfaceNames;

    /** 
    *   Parse the interfaces and return a new instance of the class
    */
    public ClassInterfaces(DataInputStream dis, ConstantPool pool) 
        throws ClassFileParserException, IOException
    {
        int i, numInterfaces;
        CPEntry tempEntry;

        numInterfaces = dis.readUnsignedShort();

        interfaces = new int[numInterfaces];
        interfaceNames = new String[numInterfaces];
            
        for (i = 0; i < numInterfaces; i++)
        {
            interfaces[i] = dis.readUnsignedShort();

            /* Read the actual value of the name for easy access later */
            try
            {
                tempEntry = pool.getEntry(interfaces[i]);

                if (tempEntry.getTagString().equals("Class"))
                {
                    interfaceNames[i] = ((ConstantClass)tempEntry).getName();
                }
                else
                {
                    throw new InvalidInterfaceException("Invalid Entry in " +
                        "interfaces. Should be of type ConstantClass.");
                }
            }
            catch (InvalidConstantPoolIndex e)
            {
                throw new InvalidInterfaceException("Invalid Entry in " +
                    "interfaces, no entry found in ConstantPool");
            }
         }
     }


    /**
    *   toString
    *   Format the fields in an easy to read format 
    */
    public String toString() 
    {
        StringBuilder s = new StringBuilder();
        ConstantClass interfaceClass;

        if ( interfaces.length == 0 )
        {
            s.append("\tNo Entries in Interfaces.");
        }
        else
        {
            s.append("Index      Entry\n" +
                     "----------------\n");

            for (int i = 0; i < interfaces.length; i++)
            {
                s.append(String.format("Index: 0x%02x\t Value: %s\n", 
                    interfaces[i], interfaceNames[i]));
            }
        }

        return s.toString();
    }
}


/**
*   Represents the situation when an interface has an invalid state
*/
class InvalidInterfaceException extends ClassFileParserException
{
    /** Passes the exception message up to the super class */
    public InvalidInterfaceException(String msg) { super(msg); }
}