/**
*   This class is used to test the functionality of the ClassFIleParser and
*   its derivative classes by having a variety of boundary cases inside
*/

public class TestClass
{
    /* These methods are intended to test when there are duplicate calls in
        one method. 

        Call Tree Should be:
        TestClass.methodA()
            TestClass.methodB()

        Total Classes: 1
        Total Unique Methods: 2
    */
    public void methodA()
    {
        methodB();
        methodB();
        methodB();
    }

    public void methodB() {}


    /* These methods are intended to test when there are overloaded methods
        which are called in one method. 

        Call Tree Should be:
        TestClass.methodF()
            TestClass.methodE()
            TestClass.methodE(int)
            TestClass.methodE(char)

        Total Classes: 1
        Total Unique Methods: 4
    */
    public void methodE() {}
    public void methodE(int i) {}
    public void methodE(char c) {}

    public void methodF()
    {
        methodE();
        methodE(0);
        methodE('c');
    }


    /* This methods is intended to test when there are classes used which
        are not located in the current folder. 

        Call Tree Should be:
        TestClass.methodG()
            Integer.Constructor(int) [missing]

        Total Classes: 2
        Total Unique Methods: 2
    */
    public void methodG()
    {
        Integer i = new Integer(0);
    }


    /* This methods is intended to test when there is recursion in the 
        call tree.

        Call Tree Should be:
        TestClass.methodH()
            TestClass.methodH() [recursive]

        Total Classes: 1
        Total Unique Methods: 1
    */
    public void methodH()
    {
        methodH();
    }


    /* These methods are intended to test when there is recursion in the 
        call tree.

        Call Tree Should be:
        TestClass.methodI()
            TestClass.methodJ()
                TestClass.methodI() [recursive]

        Total Classes: 1
        Total Unique Methods: 2
    */
    public void methodI()
    {
        methodJ();
    }

    public void methodJ()
    {
        methodI();
    }


     /* This methods is intended to test when there is use of a static 
        method in another method.

        Call Tree Should be:
        TestClass.methodK()
            Integer.getInteger(String)

        Total Classes: 2
        Total Unique Methods: 2
    */
    public void methodK()
    {
        Integer.getInteger("1");
    }


     /* This method is used to test when a slightly more complicated
        call tree occurs.

        Call Tree Should be:
        TestClass.methodL()
            TestClass.methodA()
                TestClass.methodB()
            TestClass.methodF()
                TestClass.methodE()
                TestClass.methodE(int)
                TestClass.methodE(char)
            TestClass.methodG()
                Integer.Constructor(int) [missing]
            TestClass.methodH()
                TestClass.methodH() [recursive]
            TestClass.methodI()
                TestClass.methodJ()
                    TestClass.methodI() [recursive]
            TestClass.methodK()
                Integer.getInteger(String) [missing]
            String.Constructor(String) [missing]
            Integer.getInteger(String) [missing]
            Double.parseDouble(String) [missing]
            String.substring(int, int) [missing]


        Total Classes: 4
        Total Unique Methods: 18
    */
    public void methodL()
    {
        methodA();
        methodF();
        methodG();
        methodH();
        methodI();
        methodK();

        Integer.getInteger(new String("12"));
        Double.parseDouble(new String("15"));
        String s = new String("Hello World!");
        s = s.substring(2, 8);
    }
}


/**
*   This class is used to test the functionality of detecting an abstract
*   method
*/
abstract class TestClass2
{
    /* This methods is intended to test when there is use of an abstract 
        method in another method.

        Call Tree Should be:
        TestClass2.methodA() [abstract]

        Total Classes: 1
        Total Unique Methods: 1
    */
    public abstract void methodA();
}