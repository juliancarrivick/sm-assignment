import java.io.*;
import java.util.*;




/**
*   Parses and stores the attributes from a class file, whether they be for a 
*   field, method or class.
*   @author Julian Carrivick
*   @studentid 16164442
*   @subject Software Metrics 400
*   @date May 2014
*/
public class Attributes implements Iterable<Attribute>
{
    private Attribute[] attributes;

    /**
    *   Parses the attributes, creating objects for each of the attributes and
    *   returning a new instance of the class.
    */
    public Attributes(DataInputStream dis, ConstantPool pool) 
        throws IOException, ClassFileParserException
    {
        int attributeCount;

        attributeCount = dis.readUnsignedShort();

        attributes = new Attribute[attributeCount];

        for (int i = 0; i < attributeCount; i++)
        {
            attributes[i] = Attribute.parse(dis, pool);

        }
    }

    /** 
    *   toString
    *   Returns the attributes as a formatted string 
    */
    public String toString()
    {
        StringBuilder s = new StringBuilder();

        if (attributes.length == 0)
        {
            s.append("\n\tNo attributes.");
        }
        else
        {
            for (int i = 0; i < attributes.length; i++) 
            {
                s.append("\n\t" + attributes[i].toString());
            }
        }

        return s.toString();
    }
    

    /** 
    *   iterator
    *   Returns an iterator for this object 
    */
    public Iterator<Attribute> iterator()
    {
        return new AttributeIterator();
    }



    /****************************
    *   INTERNAL ITERATOR CLASS *
    ****************************/
    private class AttributeIterator implements Iterator<Attribute>
    {
        private int iterNext;


        /**
        *   Returns a new instance of this class
        */
        public AttributeIterator()
        {
            iterNext = 0;
        }


        /**
        *   hasNext
        *   true if there is another item to iterate through
        *   false otherwise
        */
        public boolean hasNext()
        {
            return iterNext < attributes.length;
        }


        /**
        *   next
        *   iterates to the next item
        */
        public Attribute next()
        {
            Attribute value;

            if (iterNext < attributes.length)
            {
                value = attributes[iterNext];
                iterNext++;
            }
            else
            {
                value = null; 
            }

            return value;
        }


        /**
        *   remove
        *   An unsupported method which is inherited from the interface 
        *   Iterator
        */
        public void remove()
        {
            throw new UnsupportedOperationException("Remove not supported.");
        }
    }
}