#!/bin/bash

rm -rf results.txt

for choice in 2 7 8 9 10 12 13
do
	java ClassFileParser TestClass.class $choice >> results.txt
done

java ClassFileParser TestClass2.class 2 >> results.txt

difference=$(diff expected.txt results.txt)

if [ "$difference" != "" ]
then
	echo "Results are not same as expected output, please check results."
else
	echo "Results are correct."
fi