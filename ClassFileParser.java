import java.io.*;
import io.*;




/**
 * Parses and displays a Java .class file.
 *
 * @author David Cooper
 * @edited Julian Carrivick
 * @studentid 16164442
 * @date May 2014
 */
public class ClassFileParser
{
    /**
    *   main
    *   Will get run at runtime, is a GUI wrapper for the call tree
    *   functionality.
    */
    public static void main(String[] args)
    {
        ClassFile cf;
        ClassCallTree callTree;
        String fileName;
        int i, choice;
        ClassMethod[] methodIndex;
        boolean success = false;

        fileName = null;
        choice = -1;

        if (args.length == 2)
        {
            fileName = args[0];

            try
            {
                choice = Integer.parseInt(args[1]);
            }
            catch (NumberFormatException e)
            {
                System.out.println("Invalid method number.");
                choice = -1;
            }
        }
        else if(args.length == 1)
        {
            fileName = args[0];
        }
        else
        {
            System.out.println("Usage: java ClassFileParser <class-file> " +
                "[<method-number>]");
        }

        try
        {
            /* If filename is not blank, parse it and present a list of
                methods for the user to choose from */
            if ( !fileName.trim().equals(""))
            {
                cf = new ClassFile(fileName);
                callTree = new ClassCallTree(cf);
                i = 0;

                /* A quick accessor for the methods in the class */
                methodIndex = new ClassMethod[cf.getMethods().getNumMethods()];
                
                for (ClassMethod m : cf.getMethods()) 
                {
                    methodIndex[i] = m;
                    i++;    
                }

                choice = getChoice(methodIndex, choice);

                if (choice > 0)
                {
                    /* Build a call tree for the chosen method */
                    callTree.buildCallTree(methodIndex[choice - 1]);

                    System.out.println(callTree.toString());
                }
            }
        }
        catch(IOException e)
        {
            System.out.printf("Cannot read \"%s\": %s\n",
                args[0], e.getMessage());
        }
        catch(ClassFileParserException e)
        {
            System.out.printf("Class file format error in \"%s\": %s\n",
                args[0], e.getMessage());
        }
    }


    /**
    *   getChoice
    *   Gets a choice from a user about which method they want to analyse.
    */
    public static int getChoice(ClassMethod[] methods, int currChoice)
    {
        int choice = currChoice;

        while (choice < 0 || choice > methods.length)
        {
            for (int i = 0; i < methods.length; i++)
            {
                System.out.println((i + 1) + ". " + 
                    methods[i].toTypeString());    
            }

            choice = ConsoleInput.readInt("Which method would you " +
                "like analysed (0 to exit)");
        }

        return choice;
    }
}
