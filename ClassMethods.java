import java.io.*;
import java.util.*;
import java.util.regex.*;




/**
*   Parses and stores the methods from a class file.
*   @author Julian Carrivick
*   @studentid 16164442
*   @subject Software Metrics 400
*   @date May 2014
*/
public class ClassMethods implements Iterable<ClassMethod>
{
    private ClassMethod[] methods;

    /** 
    *   ClassMethods
    *   Returns a new instance of the class
    *   Parses the methods 
    */
    public ClassMethods(DataInputStream dis, ConstantPool pool) 
        throws IOException, ClassFileParserException
    {
        int numMethods;

        numMethods = dis.readUnsignedShort();

        methods = new ClassMethod[numMethods];

        for (int i = 0; i < numMethods; i++)
        {
            methods[i] = new ClassMethod(dis, pool);
        }
    }

    /** 
    *   toString
    *   Converts the methods into human readable information
    */
    public String toString()
    {
        StringBuilder s = new StringBuilder();

        if (methods.length == 0)
        {
            s.append("\tNo methods in class");
        }
        else
        {
            for (int i = 0; i < methods.length; i++)
            {
                s.append("\n" + methods[i].toString());
            }
        }

        return s.toString();
    }


    /** 
    *   getNumMethods
    *   Returns the number of methods there are 
    */
    public int getNumMethods()
    {
        return methods.length;
    }


    /** 
    *   iterator
    *   Returns an iterator for the methods 
    */
    public Iterator<ClassMethod> iterator()
    {
        return new MethodIterator();
    }


    /****************************
    *   INTERNAL ITERATOR CLASS *
    ****************************/
    private class MethodIterator implements Iterator<ClassMethod>
    {
        private int iterNext;


        /**
        *   MethodIterator
        *   Returns a new instance of the class.
        */
        public MethodIterator()
        {
            iterNext = 0;
        }


        /**
        *   hasNext
        *   true if more methods to iterate through
        *   false if not.
        */
        public boolean hasNext()
        {
            return iterNext < methods.length;
        }


        /**
        *   next
        *   Iterates to the next value
        */
        public ClassMethod next()
        {
            ClassMethod value;

            if (iterNext < methods.length)
            {
                value = methods[iterNext];
                iterNext++;
            }
            else
            {
                value = null; 
            }

            return value;
        }


        /**
        *   remove
        *   An unsupported function from the Iterator interface
        */
        public void remove()
        {
            throw new UnsupportedOperationException("Remove not supported.");
        }
    }
}

/** 
*   ClassMethod
*   Represents a single class method
*/
class ClassMethod
{
    private int accessFlags, nameIndex, descriptorIndex;
    private String name, descriptor;
    private Attributes attributes;
    private ArrayList<String> parameters;


    /**
    *   default constructor
    *   No methods will work except parseDescriptor. This is to enable 
    *   parseDescriptor to work when there isn't a valid Method Object 
    *   available
    */
    public ClassMethod()
    {
        accessFlags = nameIndex = descriptorIndex = -1;
        name = descriptor = null;
        attributes = null;
        parameters = null;
    }

    /** Parse the method */
    public ClassMethod(DataInputStream dis, ConstantPool pool)
        throws IOException, ClassFileParserException
    {
        int attributeCount;
        CPEntry tempEntry;

        accessFlags = dis.readUnsignedShort();
        nameIndex = dis.readUnsignedShort();
        descriptorIndex = dis.readUnsignedShort();

        /* Read the actual values of the name and descriptor for easy
            access later */
        try
        {
            /* Get the method name */
            tempEntry = pool.getEntry(nameIndex);

            if (tempEntry.getTagString().equals("Utf8"))
            {
                name = ((ConstantUtf8)tempEntry).getBytes();
            }
            else
            {
                throw new InvalidClassMethodException("Invalid method in" +
                    " class (not ConstantUtf8 type).");
            }


            /* Get the method descriptor (parameters and return type) */
            tempEntry = pool.getEntry(descriptorIndex);

            if (tempEntry.getTagString().equals("Utf8"))
            {
                descriptor = ((ConstantUtf8)tempEntry).getBytes();
            }
            else
            {
                throw new InvalidClassMethodException("Invalid method in" +
                    " class (not ConstantUtf8 type).");
            }
        }
        catch (InvalidConstantPoolIndex e)
        {
            throw new InvalidClassMethodException("Invalid method in" +
                " class (not found in ConstantPool).");
        }

        parameters = parseDescriptor(descriptor);

        attributes = new Attributes(dis, pool);
    }


    /** 
    *   toString
    *   A string representing the method
    */
    public String toString()
    {
        StringBuilder s = new StringBuilder();

        s.append("Name: " + name + ", Descriptor: " + descriptor);
        s.append(String.format(", Access Flags: 0x%04x", accessFlags));
        s.append(attributes.toString());

        return s.toString();
    }


    /*
    *   toTypeString
    *   Returns the method as its name and parameters.
    */
    public String toTypeString()
    {
        StringBuilder s = new StringBuilder();
        int i = 1;

        if (name.equals("<init>"))
        {
            s.append("Constructor(");
        }
        else
        {
            s.append(name + "(");
        }

        for (String p : parameters)
        {
            s.append(p);

            if (i < parameters.size())
            {
                s.append(", ");
            }

            i++;
        }

        s.append(")");

        return s.toString();
    }


    /**
    *   getMethodRefsCalled
    *   For this method, return a list of ConstantRefs which the method calls
    */
    public ArrayList<ConstantRef> getMethodRefsCalled()
        throws InvalidConstantPoolIndex, InvalidAttributeException
    {
        ArrayList<ConstantRef> methodsCalled;
        CodeAttribute code;

        methodsCalled = new ArrayList<ConstantRef>();
        code = null;

        for (Attribute attr : attributes) 
        {
             if (attr.getAttributeString().equals("Code"))
             {
                code = (CodeAttribute)attr;
                methodsCalled = code.getMethodRefsCalled();
             }   
        }

        return methodsCalled;
    }


    /**
    *   equals
    *   Checks a name and descriptor for equality with this method.
    */
    public boolean equals(String inName, String inDescriptor)
    {
        return name.equals(inName) && descriptor.equals(inDescriptor);
    }


    /**
    *   equals
    *   Checks another method for equality.
    *   Methods are equal if they have the same name and descriptor
    */
    public boolean equals(ClassMethod inMethod)
    {
        return (name.equals(inMethod.getName()) && 
                descriptor.equals(inMethod.getDescriptor()));
    }


    /** 
    *   getAttributes
    *   Returns the attributes associated with this method
    */
    public Attributes getAttributes()
    {
        return attributes;
    }


    /**
    *   getName
    *   Returns the name of this method
    */
    public String getName()
    {
        return name;
    }

    /**
    *   getDescriptor
    *   Returns the unparsed descriptor value.
    */
    public String getDescriptor()
    {
        return descriptor;
    }

    /**
    *   getAccessFlags
    *   Returns the ran integer value of the access flags for the method
    */
    public int getAccessFlags()
    {
        return accessFlags;
    }


    /**
    *   isAbstract
    *   Returns whether this method is abstract or not.
    */
    public boolean isAbstract()
    {
        boolean abstr;

        /* A method is abstract if its access flags have a value of 0x0400
            in it (ie the 11th bit is a 1)
            This can be calculated by taking bitwise-and with 0x0400 and 
            seeing if it equals 0x0400 (if not the value will be 0x0) 
            For more information see:
        http://docs.oracle.com/javase/specs/jvms/se7/html/jvms-4.html#jvms-4.6
            */
        if ( (accessFlags & 0x0400) == 0x0400)
        {
            abstr = true;
        }
        else
        {
            abstr = false;
        }

        return abstr;
    }


    /*
    *   parseDescriptor
    *   Code adapted from Vijay Krishna on Github Gist:
    *   https://gist.github.com/VijayKrishna/5180279
    *   
    *   This function takes the descriptor of a method and parses it into a
    *   list of parameters 
    */
    public ArrayList<String> parseDescriptor(String inDesc) 
        throws InvalidClassMethodException
    {
        int beginIndex, endIndex;
        Pattern pattern;
        Matcher matcher;
        String param;
        ArrayList<String> paramList;

        paramList = new ArrayList<String>();

        beginIndex = inDesc.indexOf('(');
        endIndex = inDesc.lastIndexOf(')');

        /* If there are missing brackets then something has gone horribly 
            wrong */
        if ( beginIndex == -1 || endIndex == -1)
        {
            throw new InvalidClassMethodException("Descriptor in " +
                name + " is invalid.");
        }

        param = inDesc.substring(beginIndex + 1, endIndex);

        /* This regex describes the general form of one prefix of an element 
            in a Descriptor. 
            eg. (Ljava/io/DataInputStream;LConstantPool;)V

            The full java spec can be found at:
    http://docs.oracle.com/javase/specs/jvms/se7/html/jvms-4.html#jvms-4.3.3
            */
        pattern = Pattern.compile("\\[*L[^;]+;|\\[*[ZBCSIFDJ]|[ZBCSIFDJ]");
        matcher = pattern.matcher(param);

        /* We want to get each instance of the pattern and turn it into a
            human readable form */
        while (matcher.find())
        {
            param = matcher.group();
            paramList.add(getDataType(param));
        }

        return paramList;
    }


    /**
    *   getDataType
    *   Given a descriptor stub, turn it into a human readable data type
    */
    public String getDataType(String inData) 
        throws InvalidClassMethodException
    {
        int arrayDimension;
        String dataType;
        StringBuilder s;

        s = new StringBuilder();
        arrayDimension = 0;

        while (inData.charAt(arrayDimension) == '[')
        {
            arrayDimension++;
        }

        switch (inData.charAt(arrayDimension))
        {
            case 'B': dataType = "Byte";                    break;
            case 'C': dataType = "char";                    break;
            case 'D': dataType = "double";                  break;
            case 'F': dataType = "float";                   break;
            case 'I': dataType = "int";                     break;
            case 'J': dataType = "long";                    break;
            case 'S': dataType = "short";                   break;
            case 'Z': dataType = "boolean";                 break;
            case 'L': dataType = getClassName(inData);      break;
            default:
                throw new InvalidClassMethodException("Unknown Data Type");
        }

        s.append(dataType);

        for (int i = 0; i < arrayDimension; i++) 
        {
            s.append("[]");
        }

        return s.toString();
    }


    /**
    *   getClassName
    *   Takes a descriptor stub which has already been determined to be an
    *   object type and strips the package name from the front.
    *   ie java/lang/String => String
    */
    private String getClassName(String inClassName)
    {
        String[] objPackages;
        String tempObjName;

        objPackages = inClassName.split("/");

        tempObjName = objPackages[objPackages.length - 1];

        /* If the full object name is not part of a package ie 
            Ljava/lang/String then the 'L' from the datatype (above) will not
            be excluded and so we need to get the substring from the second
            caharacter */
        if (objPackages.length == 1)
        {
            if (objPackages[0].charAt(0) == '[')
            {
                tempObjName = 
                    tempObjName.substring(2, tempObjName.length() - 1);
            }
            else
            {
                tempObjName = 
                    tempObjName.substring(1, tempObjName.length() - 1);
            }
        }
        else
        {
            tempObjName = tempObjName.substring(0, tempObjName.length() - 1);
        }

        return tempObjName;
    }
}


/**
*   This exception describes the condition where a method from a class file
*   has an invalid state
*/
class InvalidClassMethodException extends ClassFileParserException
{
    /** Passes the exception message up to the super class */
    public InvalidClassMethodException(String msg) { super(msg); }
}