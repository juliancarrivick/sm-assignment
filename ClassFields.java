import java.io.*;




/**
*   Parses and stores the fields from a class file.
*   @author Julian Carrivick
*   @studentid 16164442
*   @subject Software Metrics 400
*   @date May 2014
*/
public class ClassFields
{
    private ClassField[] fields;

    /** 
    *   Parse the fields and return a new instance of the object
    */
    public ClassFields(DataInputStream dis, ConstantPool pool) 
        throws ClassFileParserException, IOException
    {
        int numFields;

        numFields = dis.readUnsignedShort();

        fields = new ClassField[numFields];

        for (int i = 0; i < numFields; i++)
        {
            fields[i] = new ClassField(dis, pool);
        }
    }


    /** 
    *   toString
    *   Format the fields in an easy to read format 
    */
    public String toString()
    {
        StringBuilder s = new StringBuilder();

        if (fields.length == 0)
        {
            s.append("\tNo fields in class");
        }
        else
        {
            for (int i = 0; i < fields.length; i++)
            {
                s.append("\n" + fields[i].toString());
            }
        }

        return s.toString();
    }
}


/** 
*   Represents a single field in a class 
*/
class ClassField
{
    int accessFlags, nameIndex, descriptorIndex;
    String name, descriptor;
    Attributes attributes;

    /**
    *   Parse the fields and return a new instance of the class
    */
    public ClassField(DataInputStream dis, ConstantPool pool ) 
        throws ClassFileParserException, IOException
    {
        int attributeCount;
        CPEntry tempEntry;

        accessFlags = dis.readUnsignedShort();
        nameIndex = dis.readUnsignedShort();
        descriptorIndex = dis.readUnsignedShort();

        /* Read the actual values of the name and descriptor for easy
            access later */
        try
        {
            tempEntry = pool.getEntry(nameIndex);

            if (tempEntry.getTagString().equals("Utf8"))
            {
                name = ((ConstantUtf8)tempEntry).getBytes();
            }
            else
            {
                throw new InvalidClassFieldException("Invalid field in" +
                " class (not ConstantUtf8 type)."); 
            }

            tempEntry = pool.getEntry(descriptorIndex);

            if (tempEntry.getTagString().equals("Utf8"))
            {
                descriptor = ((ConstantUtf8)tempEntry).getBytes();
            }
            else
            {
                throw new InvalidClassFieldException("Invalid field in" +
                    " class (not ConstantUtf8 type)."); 
            }
        }
        catch (InvalidConstantPoolIndex e)
        {
            throw new InvalidClassFieldException("Invalid field in" +
                " class (not found in ConstantPool).");
        }

        attributes = new Attributes(dis, pool);
    }

    /** 
    *   toString
    *   Format the fields in an easy to read format 
    */
    public String toString()
    {
        StringBuilder s = new StringBuilder();

        s.append("Name: " + name + ", Descriptor: " + descriptor);
        s.append(String.format(", Access Flags: 0x%04x", accessFlags));
        s.append(attributes.toString());

        return s.toString();
    }
}


/**
*   Represents the situation where a class field has a invalid state.
*/
class InvalidClassFieldException extends ClassFileParserException
{
    /** Passes the exception message up to the super class */
    public InvalidClassFieldException(String msg) { super(msg); }
}