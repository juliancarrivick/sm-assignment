import java.io.*;
import java.util.*;




/**
*   Parses and stores an attribute value from a java .class file.
*   @author Julian Carrivick
*   @studentid 16164442
*   @subject Software Metrics 400
*   @date May 2014
*/
public abstract class Attribute
{
    /**
    *   parse
    *   A static method to parse the DataInputStream, work out what type of
    *   attribute it is, and return the relevent Attribute Object.
    *   At this point, only the Code Attribute is parsed.
    */
    public static Attribute parse(DataInputStream dis, ConstantPool pool) 
        throws IOException, ClassFileParserException
    {
        ConstantUtf8 tempEntry;
        Attribute attribute;
        int nameIndex;
        int attributeLength;
        String name;

        nameIndex = dis.readUnsignedShort();

        try
        {
            tempEntry = (ConstantUtf8)pool.getEntry(nameIndex);
            name = tempEntry.getBytes();

            /* Depending on the name, create the relevent Attribute type */
            if (name.equals("Code"))
            {
                attribute = new CodeAttribute(dis, pool);
            }
            else
            {
                attribute = new MiscAttribute(dis, pool, name);
            }
        }
        catch (ClassCastException e)
        {
            throw new InvalidAttributeException("Invalid attribute in" +
                " class (not ConstantUtf8 type).");
        }
        catch (InvalidConstantPoolIndex e)
        {
            throw new InvalidAttributeException("Invalid attribute in" +
                " class (not found in ConstantPool).");
        }

        return attribute;
    }


    /**
    *   getAttributeString
    *   All inheriting classes must have this method
    */
    public abstract String getAttributeString();
}


/**
*   A Miscellaneous Attribute, if a predefined one is not found, this is used
*   instead
*/
class MiscAttribute extends Attribute
{
    private int nameIndex;
    private String attributeName;
    private byte[] data;


    /**
    *   Returns a new instance of the class
    */
    public MiscAttribute(DataInputStream dis, ConstantPool pool, String name)
        throws IOException, InvalidAttributeException
    {
        int attributeLength;

        attributeName = name;

        attributeLength = 
            dis.readUnsignedShort() << 16 | dis.readUnsignedShort();

        if ( attributeLength < 0 )
        {
            throw new InvalidAttributeException("Attribute Length" +
                " is larger than 2GB, not supported");
        }

        data = new byte[attributeLength];

        for (int i = 0; i < attributeLength; i++)
        {
            data[i] = dis.readByte();
        }
    }

    /**
    *   getAttributeString
    *   Returns the attribute's name
    */
    public String getAttributeString() { return attributeName; }
}


/** 
*   Represents a Code Attribute - holding the byte code that actually gets
*   run!
*/
class CodeAttribute extends Attribute 
{
    private int length, maxStack, maxLocals, codeLength, exceptionTableLength;
    private byte[] code;
    private Exception[] exceptions;
    private Attributes attributes;
    private int numInstructions;
    private Instruction[] instructions;
    private ConstantPool constantPool;


    /**
    *   Returns a new CodeAttribute instance
    */
    public CodeAttribute(DataInputStream dis, ConstantPool pool) 
        throws IOException, ClassFileParserException
    {
        int i;

        constantPool = pool;

        /* Length isn't really needed for a Code Attribute as the rest of the 
            attribute has explicit lengths, but it provides a useful
            check to see if things have gone wrong (by overflowing) */
        length = (dis.readUnsignedShort() << 16) | dis.readUnsignedShort();

        if (length < 0)
        {
            throw new InvalidAttributeException("Attribute Length" +
                " is larger than 2GB, not supported");
        }

        maxStack = dis.readUnsignedShort();
        maxLocals = dis.readUnsignedShort();
        codeLength = dis.readUnsignedShort() << 16 | dis.readUnsignedShort();

        if (codeLength < 0)
        {
            throw new InvalidAttributeException("Code Length" +
                " is larger than 2GB, not supported");
        }

        /* The number of instructions is probably lower than codeLength due to
            the extra bytes that some instructions need but this is an upper 
            bound */
        instructions = new Instruction[codeLength];
        code = new byte[codeLength];

        for (i = 0; i < codeLength; i++)
        {
            code[i] = dis.readByte();
        }

        /* Resetting the values in preparation for use in the next loop */
        i = numInstructions = 0;

        while (i < codeLength)
        {
            instructions[numInstructions] = new Instruction(code, i);

            /* Increment i by the number of bytes that this instruction
                uses (as it can be variable) */
            i += instructions[numInstructions].getSize();
            numInstructions++;
        }

        exceptionTableLength = dis.readUnsignedShort();
        exceptions = new Exception[exceptionTableLength];

        for (i = 0; i < exceptionTableLength; i++) 
        {
            exceptions[i] = new Exception();

            exceptions[i].start_pc = dis.readUnsignedShort();
            exceptions[i].end_pc = dis.readUnsignedShort();
            exceptions[i].handler_pc = dis.readUnsignedShort();
            exceptions[i].catchType = dis.readUnsignedShort();
        }

        attributes = new Attributes(dis, pool);
    }


    /**
    *   toString
    *   Returns the Code Attribute in a formatted string
    */
    public String toString()
    {
        StringBuilder s;

        s = new StringBuilder();

        s.append("Code:");

        for (int i = 0; i < instructions.length; i++) 
        {
            if (instructions[i] != null )
            {
                s.append("\n\t  " + instructions[i].toString() );
            }
        }

        return s.toString();
    }


    /**
    *   getAttributeString
    *   Returns the Attribute type, in this case: code
    */
    public String getAttributeString() { return "Code"; }


    /**
    *   getMethodRefsCalled
    *   Returns the ConstantRefs that this Code attribute calls
    */
    public ArrayList<ConstantRef> getMethodRefsCalled()
        throws InvalidConstantPoolIndex, InvalidAttributeException
    {
        ArrayList<ConstantRef> methodsCalled;
        Instruction tempInstruction;
        byte[] extraBytes;
        int constantPoolIndex;
        CPEntry tempEntry;
        ConstantRef methodRef;
        String instructionName;

        methodsCalled = new ArrayList<ConstantRef>();

        /* For each instruction that is an invoke instruction (excluding
        *   invokedynamic) lookup the relevent constant pool entry and add it
        *   to the list */
        for (int i = 0; i < numInstructions; i++) 
        {
            tempInstruction = instructions[i];

            instructionName = tempInstruction.getOpcode().getMnemonic();

            /* If an instruction is of these types, we want to see what 
                functions they call */
            if (instructionName.equals("invokevirtual") ||
                instructionName.equals("invokespecial") ||
                instructionName.equals("invokestatic") ||
                instructionName.equals("invokeinterface"))
            {
                extraBytes = tempInstruction.getExtraBytes();

                /* Need to bitwise-and with 0xFF as java considers bytes
                signed and it can make constantPoolIndex negative */
                constantPoolIndex = ((int)(extraBytes[0] & 0xFF) << 8) | 
                                     (int)(extraBytes[1] & 0xFF);
                tempEntry = constantPool.getEntry(constantPoolIndex);

                /* This allows us to assume that all ConstantRef object from
                    now are actually MethodRefs */
                if (tempEntry.getTagString().equals("Methodref") ||
                    tempEntry.getTagString().equals("InterfaceMethodref"))
                {
                    methodRef = (ConstantRef)tempEntry;
                    methodsCalled.add(methodRef);
                }
                else
                {
                    throw new InvalidAttributeException("Methods called" +
                        " must be of type ConstantRef. This is " + 
                        tempEntry.getTagString());
                }
            }
        }

        return methodsCalled;
    } 


    /**
    *   A simple container class to hold the Exception information for a Code
    *   Attribute
    */
    private class Exception
    {
        private int start_pc, end_pc, handler_pc, catchType;
    }
}


/**
*   An class that indicates that the Attribute is invalid
*/
class InvalidAttributeException extends ClassFileParserException
{
    /** Passes the exception message up to the super class */
    public InvalidAttributeException(String msg) { super(msg); }
}